drop table if exists word_translation;
drop table if exists word;

drop table if exists word_collection;
drop table if exists word_collection_subject;
drop table if exists word_collection_word;
drop table if exists translation;

create table if not exists translation (
    id int,
    lang varchar(2) not null,
    translation varchar(1024) not null,
    primary key (id, lang)
);

create table if not exists word (
    id serial primary key, 
    description varchar(500) not null
);

create table if not exists word_translation (
    id serial primary key,
    word_id int 
        references word(id)
        on delete cascade,
    lang varchar(2) not null,
    translation varchar(1024) not null,
    unique(lang, translation)
);

create table if not exists word_collection_subject (
    id serial primary key,
    title_translation_id int not null,
    description varchar(512)
);

create table if not exists word_collection (
    id serial primary key,
    subject_id int
        references word_collection_subject(id),
    title_translation_id int not null,
    preview text not null,
    description varchar(512)
);

create table if not exists word_collection_word (
    id serial primary key,
    word_id int 
        references word(id),
    word_collection_id int
        references word_collection(id)
);


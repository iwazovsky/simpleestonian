//
//  SimpleStorage.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 13.06.2023.
//

import Foundation

protocol ISimpleStorage {

    func set<T>(_ value: T, forKey key: String)
    func get<T>(_ key: String) -> T

    func setDefaults(_ defaults: [String: Any])

}

final class UserDefaultsStorage: ISimpleStorage {
    init() {
        setDefaults([
            "nativeLanguage": "ru",
            "isSoundOn": true,

            "isDarkModeEnabled": false,
            "useSystemSettings": true
        ])
    }

    func set<T>(_ value: T, forKey key: String) {
        UserDefaults.standard.set(value, forKey: key)
    }

    func get<T>(_ key: String) -> T {
        guard let value = UserDefaults.standard.value(forKey: key) as? T else {
            fatalError("UserDefaults value is not set or wrong typed. \(key) \(T.self)")
        }

        return value
    }

    func setDefaults(_ defaults: [String : Any]) {
        UserDefaults.standard.register(defaults: defaults)
    }
}

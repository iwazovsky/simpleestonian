//
//  Locale+Region.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 08.10.2023.
//

import Foundation

extension Locale {
    static var currentLanguage: String {
        if #available(iOS 16.0, *) {
            return Self.current.language.languageCode!.identifier
        } else {
            return Self.current.languageCode!
        }
    }
}

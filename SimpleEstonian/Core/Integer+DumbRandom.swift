//
//  Integer+DumbRandom.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 28.05.2023.
//

import Foundation

extension Int {
    static func dumbRandom() -> Int {
        return Int.random(in: 1...Int.max)
    }
}

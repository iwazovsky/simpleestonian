//
//  JSONFileReader.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 29.04.2023.
//

import Foundation

class JSONFileReader {
    static var shared = JSONFileReader()

    static func load<T: Decodable>(fileName: String, fromSnakeCase: Bool = true) -> [T] {
        let path = Bundle.main.url(forResource: fileName, withExtension: "json")
        guard let path = path else { fatalError("Required data file is not found") }

        do {
            let data = try Data(contentsOf: path)
            let decoder = JSONDecoder()
            if fromSnakeCase {
                decoder.keyDecodingStrategy = .convertFromSnakeCase
            }
            return try decoder.decode([T].self, from: data)
        } catch let DecodingError.dataCorrupted(context) {
            print(context)
        } catch let DecodingError.keyNotFound(key, context) {
            print("Key '\(key)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch let DecodingError.valueNotFound(value, context) {
            print("Value '\(value)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch let DecodingError.typeMismatch(type, context)  {
            print("Type '\(type)' mismatch:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch {
            print("error: ", error)
        }

        return []
    }
}

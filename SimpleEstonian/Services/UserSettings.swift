//
//  UserSettings.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 14.09.2023.
//

import SwiftUI

class UserSettings: ObservableObject {
    private var simpleStorage: ISimpleStorage = UserDefaultsStorage()

    enum Keys {
        
    }

    @Published var language: Language?
    @Published var isDarkModeEnabled: Bool
    @Published var useSystemSettings: Bool?

    init() {
        isDarkModeEnabled = simpleStorage.get("isDarkModeEnabled")
        useSystemSettings = simpleStorage.get("useSystemStorage")
    }

    func setDarkMode(_ enabled: Bool) {
        print("Change dark mode value \(enabled)")
        isDarkModeEnabled = enabled
        self.objectWillChange.send()
    }
}

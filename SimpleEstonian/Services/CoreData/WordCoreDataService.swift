//
//  WordService.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 18.04.2023.
//

import Foundation
import CoreData


class WordCoreDataService: IWordStore, ObservableObject {
    private static let wordCoreDataStore = WordCoreDataStore()
    
    func getSubjects() -> [WordCollectionSubject] {
        var subjects: [WordCollectionSubject] = []
        let context = WordCoreDataStore.persistentContainer.viewContext

        let fetchRequest = CDWordCollectionSubject.fetchRequest()
        do {
            let cdSubjects = try context.fetch(fetchRequest)
            for subject in cdSubjects {
                subjects.append(subject.convertToCustomEntity())
            }
        } catch {
            fatalError("Error on subjects retrieve")
        }

        return subjects
    }

    func changeUserLanguage(_ nativeLanguage: Language) {
        
        
        Self.wordCoreDataStore.purgeData()
        Self.wordCoreDataStore.loadSubjects(nativeLang: nativeLanguage.a2, foreignLang: "et")
    }
    
    func changeUserLanguage(_ nativeLanguage: String) {
        Self.wordCoreDataStore.purgeData()

        var language: SupportedLanguage = .english
        if let userLanguage = SupportedLanguage(rawValue: nativeLanguage) {
            language = userLanguage
        }
        
        Self.wordCoreDataStore.loadSubjects(nativeLang: language.rawValue, foreignLang: "et")
    }
}


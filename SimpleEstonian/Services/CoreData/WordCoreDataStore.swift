//
//  CoreDataStore.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 30.04.2023.
//

import CoreData

class WordCoreDataStore {
    private let jsonFileReader = JSONFileReader.shared

    static var persistentContainer: NSPersistentContainer = {
        let persistentContainer = NSPersistentContainer(name: "SimpleEstonianModel")

        persistentContainer.loadPersistentStores { desc, error in
            if let error = error {
                print("Failed to load persistent store. \(error)")
            }
        }

        return persistentContainer
    }()

    func purgeData() {
        let context = Self.persistentContainer.viewContext
        for entity in Self.persistentContainer.managedObjectModel.entities {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity.name!)
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

            do {
                try context.execute(deleteRequest)
            } catch {
                print("Batch delete failed. \(error.localizedDescription)")
            }
        }

        do {
            try context.save()
        } catch {
            print("Context save failed. \(error.localizedDescription)")
        }
    }

    func loadSubjects(nativeLang: String, foreignLang: String) {
        
        let translations: [TranslationRaw] = JSONFileReader.load(fileName: "translation")
        let subjectsRaw: [WordCollectionSubjectRaw] = JSONFileReader.load(fileName: "word_collection_subject")
        let wordsRaw: [WordRaw] = JSONFileReader.load(fileName: "word")
        let wordTranslations: [WordTranslationRaw] = JSONFileReader.load(fileName: "word_translation")
        let collectionsRaw: [WordCollectionRaw] = JSONFileReader.load(fileName: "word_collection")
        let wordCollectionWordsRaw: [WordCollectionWordRaw] = JSONFileReader.load(fileName: "word_collection_word")

        let context = Self.persistentContainer.viewContext

        for raw in subjectsRaw {
            let subject = CDWordCollectionSubject(context: context)
            subject.id = Int16(raw.id)
            let translation = translations.first { $0.id == raw.titleTranslationId && $0.lang == nativeLang }
            if let translation = translation  {
                subject.title = translation.translation
            }

            subject.collections = NSSet()
            do {
                try context.save()
            } catch {
                print("Error on context save while parsing subjects")
            }
        }

        for raw in wordsRaw {
            let word = CDWord(context: context)
            word.id = Int32(raw.id)

            let foreignRaw = wordTranslations.first { $0.wordId == raw.id && $0.lang == foreignLang }
            guard let foreignRaw = foreignRaw else {
                print("Foreign raw translation not found")
                continue
            }
            let nativeRaw = wordTranslations.first { $0.wordId == raw.id && $0.lang == nativeLang }
            guard let nativeRaw = nativeRaw else {
                print("Native raw translation not found")
                continue
            }
            let foreignWordTranslation = CDWordTranslation(context: context)
            foreignWordTranslation.id = Int32(foreignRaw.id)
            foreignWordTranslation.lang = foreignRaw.lang
            foreignWordTranslation.translation = foreignRaw.translation

            let nativeWordTranslation = CDWordTranslation(context: context)
            nativeWordTranslation.id = Int32(nativeRaw.id)
            nativeWordTranslation.lang = nativeRaw.lang
            nativeWordTranslation.translation = nativeRaw.translation

            word.nativeWordTranslation = nativeWordTranslation
            word.foreignWordTranslation = foreignWordTranslation
            do {
                try context.save()
            } catch {
                print("Error on context save while parsing words \(error.localizedDescription)")
            }
        }

        for raw in collectionsRaw {
            let collection = CDWordCollection(context: context)

            collection.id = Int16(raw.id)
            let translation = translations.first { $0.id == raw.titleTranslationId && $0.lang == nativeLang }
            if let translation = translation  {
                collection.title = translation.translation
            }
            collection.preview = raw.preview

            var words = [CDWord]()
            for wordConnection in wordCollectionWordsRaw {
                if wordConnection.wordCollectionId != raw.id {
                    continue
                }

                let fetchRequest = CDWord.fetchRequest()
                fetchRequest.predicate = NSPredicate(format: "id == %@", argumentArray: [wordConnection.wordId])
                fetchRequest.fetchLimit = 1
                do {
                    let result = try context.fetch(fetchRequest)
                    guard let result = result.first else {
                        print("Word is not presented in db")
                        continue
                    }
                    words.append(result)
                } catch {
                    print("Word fetch request failed")
                }
            }

            collection.words = NSSet(array: words)

            let fetchRequest = CDWordCollectionSubject.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "id == %@", argumentArray: [raw.subjectId])
            fetchRequest.fetchLimit = 1
            do {
                let result = try context.fetch(fetchRequest)
                guard let result = result.first else {
                    print("Subject collection fetch failed")
                    continue
                }
                collection.subject = result
            } catch {
                print("Subject collection fetch failed")
            }


            do {
                try context.save()
            } catch {
                print("Error on context save while parsing collections. Error: \(error.localizedDescription)")
            }
        }
    }


    struct TranslationRaw: Decodable {
        let id: Int
        let lang: String
        let translation: String
    }

    struct WordCollectionSubjectRaw: Decodable {
        let id: Int
        let titleTranslationId: Int
        let description: String
    }

    struct WordCollectionRaw: Decodable {
        let id: Int
        let subjectId: Int
        let titleTranslationId: Int
        let preview: String
    }

    struct WordRaw: Decodable {
        let id: Int
    }

    struct WordTranslationRaw: Decodable {
        let id: Int
        let wordId: Int
        let translation: String
        let lang: String
    }

    struct WordCollectionWordRaw: Decodable {
        let id: Int
        let wordId: Int
        let wordCollectionId: Int
    }
}

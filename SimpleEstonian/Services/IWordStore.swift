//
//  IWordService.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 01.05.2023.
//

import Foundation

protocol IWordStore: ObservableObject {
    func getSubjects() -> [WordCollectionSubject]
    func changeUserLanguage(_ nativeLanguage: Language)
    func changeUserLanguage(_ nativeLanguage: String)
}

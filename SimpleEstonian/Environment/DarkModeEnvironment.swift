//
//  DarkModeEnvironment.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 14.09.2023.
//

import SwiftUI

struct DarkModeEnvironment: EnvironmentKey {
    static var defaultValue: Bool = false
}

extension EnvironmentValues {
    var isDarkModeEnabled: Bool {
        get {
            self[DarkModeEnvironment.self]
        }
        set {
            self[DarkModeEnvironment.self] = newValue
        }
    }
}

//
//  SimpleStorageEnvironment.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 13.06.2023.
//

import SwiftUI

struct SimpleStorageEnvironmentKey: EnvironmentKey {
    static var defaultValue: any ISimpleStorage = UserDefaultsStorage()
}

extension EnvironmentValues {
    var simpleStorage: any ISimpleStorage {
        get {
            self[SimpleStorageEnvironmentKey.self]
        }
        set {
            self[SimpleStorageEnvironmentKey.self] = newValue as! UserDefaultsStorage
        }
    }
}

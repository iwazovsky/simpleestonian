//
//  WordServiceEnvironment.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 11.06.2023.
//

import SwiftUI

struct WordServiceEnvironmentKey: EnvironmentKey {
    static var defaultValue: any IWordStore = WordCoreDataService()
}

extension EnvironmentValues {
    var wordService: any IWordStore {
        get {
            self[WordServiceEnvironmentKey.self]
        }
        set {
            self[WordServiceEnvironmentKey.self] = newValue as! WordCoreDataService
        }
    }
}


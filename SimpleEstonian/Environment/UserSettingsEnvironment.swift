//
//  UserSettingsEnvironment.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 14.09.2023.
//

import SwiftUI

struct UserSettingsEnvironment: EnvironmentKey {
    static var defaultValue = UserSettings()
}

extension EnvironmentValues {
    var userSettings: UserSettings {
        get {
            self[UserSettingsEnvironment.self]
        }
        set {
            self[UserSettingsEnvironment.self] = newValue
        }
    }
}

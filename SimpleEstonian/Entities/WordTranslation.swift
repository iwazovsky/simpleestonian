//
//  WordTranslation.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 28.05.2023.
//

import Foundation

struct WordTranslation: Identifiable {
    let id: Int
    let lang: String
    let translation: String
}

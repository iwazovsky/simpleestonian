//
//  WordCollection.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 28.05.2023.
//

import Foundation

struct WordCollection: Identifiable {
    let id: Int
    let preview: String
    let title: String
    let words: [Word]
}

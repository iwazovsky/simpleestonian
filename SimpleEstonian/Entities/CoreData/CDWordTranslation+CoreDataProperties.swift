//
//  CDWordTranslation+CoreDataProperties.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 28.05.2023.
//
//

import Foundation
import CoreData


extension CDWordTranslation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDWordTranslation> {
        return NSFetchRequest<CDWordTranslation>(entityName: "CDWordTranslation")
    }

    @NSManaged public var id: Int32
    @NSManaged public var lang: String
    @NSManaged public var translation: String

}

extension CDWordTranslation : Identifiable {

}

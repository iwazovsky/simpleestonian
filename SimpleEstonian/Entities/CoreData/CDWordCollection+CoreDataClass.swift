//
//  CDWordCollection+CoreDataClass.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 28.05.2023.
//
//

import Foundation
import CoreData


public class CDWordCollection: NSManagedObject {

}

extension CDWordCollection: ICustomEntity {
    func convertToCustomEntity() -> WordCollection {
        var words = [Word]()

        for word in self.words.allObjects {
            guard let word = word as? CDWord else {
                continue
            }

            words.append(word.convertToCustomEntity())
        }

        return WordCollection(id: Int(self.id), preview: self.preview, title: self.title, words: words)
    }
}

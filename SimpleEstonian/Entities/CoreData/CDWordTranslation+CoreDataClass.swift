//
//  CDWordTranslation+CoreDataClass.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 28.05.2023.
//
//

import Foundation
import CoreData


public class CDWordTranslation: NSManagedObject {

}

extension CDWordTranslation: ICustomEntity {
    func convertToCustomEntity() -> WordTranslation {
        return WordTranslation(id: Int(self.id), lang: self.lang, translation: self.translation)
    }
}

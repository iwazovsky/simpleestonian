//
//  CDWord+CoreDataProperties.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 28.05.2023.
//
//

import Foundation
import CoreData


extension CDWord {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDWord> {
        return NSFetchRequest<CDWord>(entityName: "CDWord")
    }

    @NSManaged public var id: Int32
    @NSManaged public var foreignWordTranslation: CDWordTranslation
    @NSManaged public var nativeWordTranslation: CDWordTranslation

}

extension CDWord : Identifiable {

}

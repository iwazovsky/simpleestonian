//
//  CDWord+CoreDataClass.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 28.05.2023.
//
//

import Foundation
import CoreData


public class CDWord: NSManagedObject {

}

extension CDWord: ICustomEntity {
    func convertToCustomEntity() -> Word {
        return Word(id: Int(self.id), foreignWordTranslation: self.foreignWordTranslation.convertToCustomEntity(), nativeWordTranslation: self.nativeWordTranslation.convertToCustomEntity())
    }
}

//
//  CDWordCollectionSubject+CoreDataProperties.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 28.05.2023.
//
//

import Foundation
import CoreData


extension CDWordCollectionSubject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDWordCollectionSubject> {
        return NSFetchRequest<CDWordCollectionSubject>(entityName: "CDWordCollectionSubject")
    }

    @NSManaged public var id: Int16
    @NSManaged public var title: String
    @NSManaged public var collections: NSSet

}

// MARK: Generated accessors for collections
extension CDWordCollectionSubject {

    @objc(addCollectionsObject:)
    @NSManaged public func addToCollections(_ value: CDWordCollection)

    @objc(removeCollectionsObject:)
    @NSManaged public func removeFromCollections(_ value: CDWordCollection)

    @objc(addCollections:)
    @NSManaged public func addToCollections(_ values: NSSet)

    @objc(removeCollections:)
    @NSManaged public func removeFromCollections(_ values: NSSet)

}

extension CDWordCollectionSubject : Identifiable {

}

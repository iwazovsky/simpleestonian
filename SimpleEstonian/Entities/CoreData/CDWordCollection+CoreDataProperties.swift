//
//  CDWordCollection+CoreDataProperties.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 28.05.2023.
//
//

import Foundation
import CoreData


extension CDWordCollection {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDWordCollection> {
        return NSFetchRequest<CDWordCollection>(entityName: "CDWordCollection")
    }

    @NSManaged public var id: Int16
    @NSManaged public var preview: String
    @NSManaged public var title: String
    @NSManaged public var subject: CDWordCollectionSubject
    @NSManaged public var words: NSSet

}

// MARK: Generated accessors for words
extension CDWordCollection {

    @objc(addWordsObject:)
    @NSManaged public func addToWords(_ value: CDWord)

    @objc(removeWordsObject:)
    @NSManaged public func removeFromWords(_ value: CDWord)

    @objc(addWords:)
    @NSManaged public func addToWords(_ values: NSSet)

    @objc(removeWords:)
    @NSManaged public func removeFromWords(_ values: NSSet)

}

extension CDWordCollection : Identifiable {

}

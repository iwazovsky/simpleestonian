//
//  CDWordCollectionSubject+CoreDataClass.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 28.05.2023.
//
//

import Foundation
import CoreData


public class CDWordCollectionSubject: NSManagedObject {

}

extension CDWordCollectionSubject: ICustomEntity {
    func convertToCustomEntity() -> WordCollectionSubject {
        var collections = [WordCollection]()
        for collection in self.collections {
            guard let collection = collection as? CDWordCollection else {
                continue
            }
            collections.append(collection.convertToCustomEntity())
        }
        return WordCollectionSubject(id: Int(self.id), title: self.title, collections: collections)
    }
}

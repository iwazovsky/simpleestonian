//
//  Word.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 28.05.2023.
//

import Foundation

struct Word: Identifiable {
    let id: Int
    let foreignWordTranslation: WordTranslation
    let nativeWordTranslation: WordTranslation
}

//
//  CustomEntity.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 28.05.2023.
//

import Foundation

protocol ICustomEntity {
    associatedtype T

    func convertToCustomEntity() -> T
}

//
//  SupportedLanguage.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 08.10.2023.
//

import Foundation

enum SupportedLanguage: String {
    case english = "en"
    case russian = "ru"
    case ukrainian = "uk"
}

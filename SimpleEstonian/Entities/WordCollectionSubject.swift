//
//  WordCollectionSubject.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 28.05.2023.
//

import Foundation

struct WordCollectionSubject: Identifiable {
    let id: Int
    let title: String
    let collections: [WordCollection]
}

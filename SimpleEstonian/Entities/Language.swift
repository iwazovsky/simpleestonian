//
//  Language.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 11.06.2023.
//

import Foundation

 struct Language: Identifiable, Hashable, Codable {
    let id: UUID = UUID()
    let a2: String
    let name: String
}


import SwiftUI
import CoreData

@main
struct SimpleEstonianApp: App {

    private let simpleStorage: ISimpleStorage = UserDefaultsStorage()
    private let wordService: any IWordStore = WordCoreDataService()
    private let userSettings = UserSettings()
    @StateObject var colorSchemeManager = ColorSchemeManager()
    
    @AppStorage("userLanguage") var userLanguage: String = ""

    init() {
        warmUp()
    }
    
    func warmUp() {
        let currentLanguage = Locale.currentLanguage
        
        if userLanguage != currentLanguage {
            wordService.changeUserLanguage(currentLanguage)
            userLanguage = currentLanguage
        }
    }

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.simpleStorage, simpleStorage)
                .environment(\.wordService, wordService)
                .environment(\.userSettings, userSettings)
                .environmentObject(colorSchemeManager)
                .onAppear {
                    UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
                    colorSchemeManager.applyColorScheme()
                }
        }
    }
}


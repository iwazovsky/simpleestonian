
import SwiftUI

struct ContentView: View {
    @Environment(\.wordService) private var wordService: any IWordStore
    @Environment(\.userSettings) var userSettings: UserSettings
    @Environment(\.locale) var locale

    @StateObject private var viewModel = ViewModel()

    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                Spacer()

                HStack(alignment: .top) {
                    VStack(alignment: .leading) {
                        Text("main_greetings")
                            .font(.custom("SFProRounded-Bold", size: 34))
                            .bold()
                            .foregroundColor(Color("TextPrimary"))
                    }
                    .padding(.horizontal)

                    Spacer()
                    NavigationLink(destination: SettingsView()) {
                        Image(systemName: "gearshape")
                            .resizable()
                            .font(.system(size: 18, weight: .bold))
                            .frame(width: 18, height: 18)

                    }
                    .padding(13)
                    .background(Color("ButtonBg"))
                    .foregroundColor(Color("TextPrimary"))
                    .cornerRadius(24)
                    .padding(.trailing)


                }
                .padding(.top)

                if !viewModel.subjects.isEmpty {
                    WordCollectionsView(subjects: viewModel.subjects)
                }
            }
            .onAppear {
                viewModel.setup(wordService: wordService)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {

    static let subjects = [
        WordCollectionSubject(id: 1, title: "Food", collections: [
            WordCollection(id: 2, preview: "🍭", title: "Sweets", words: []),
            WordCollection(id: 3, preview: "🍊", title: "Fruits", words: [])
        ]),
        WordCollectionSubject(id: 2, title: "Animals", collections: [
            WordCollection(id: 1, preview: "🐶", title: "Mammals", words: [])
        ]),
    ]

    static var previews: some View {
        ContentView()
    }
}

//
//  SettingsView+ViewModel.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 04.09.2023.
//

import SwiftUI

extension SettingsView {
    final class ViewModel: ObservableObject {
        private var wordService: (any IWordStore)?
        private var colorSchemeManager: ColorSchemeManager?
        @Published var userSettings: UserSettings?
        @Published var subjects: [WordCollectionSubject] = []
        @Published var selectedLanguage: Language = Language(a2: "en", name: "English") {
            didSet {
                wordService?.changeUserLanguage(selectedLanguage)
                nativeLanguage = selectedLanguage.a2
            }
        }

        @AppStorage("nativeLanguage") var nativeLanguage: String = "en"

        @AppStorage("language") var language: String = ""

        @AppStorage("isDarkModeEnabled") var isDarkModeEnabled: Bool = false {
            didSet {
                updateColorSchema()
            }
        }
        @AppStorage("useSystemSettings") var useSystemSettings: Bool = false {
            didSet {
                updateColorSchema()
            }
        }
    

        var languages: [Language] = [
            Language(a2: "en", name: "English"),
            Language(a2: "ru", name: "Russian"),
            Language(a2: "ua", name: "Ukranian")
        ]

        func setup(wordService: any IWordStore, userSettings: UserSettings, colorSchemeManager: ColorSchemeManager) {
            self.wordService = wordService
            self.userSettings = userSettings
            self.colorSchemeManager = colorSchemeManager

            if let selectedLanguage = languages.first(where: {$0.a2 == nativeLanguage}) {
                self.selectedLanguage = selectedLanguage
            }
        }

        func updateColorSchema() {
            var mode = ColorScheme.unspecified
            if !useSystemSettings {
                mode = isDarkModeEnabled ? ColorScheme.dark : ColorScheme.light
            }
            colorSchemeManager?.colorScheme = mode
        }
    }
}

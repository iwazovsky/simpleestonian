//
//  WordCollectionChipView+ViewModel.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 04.09.2023.
//

import SwiftUI

extension WordCollectionChipView {
    @MainActor final class ViewModel: ObservableObject {
        var collection: WordCollection

        init(collection: WordCollection) {
            self.collection = collection
        }
    }
}

//
//  CollectionGroupView.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 30.03.2023.
//

import SwiftUI

struct WordCollectionsView: View {
    @StateObject private var viewModel: ViewModel

    @MainActor
    init(subjects: [WordCollectionSubject]) {
        _viewModel = StateObject(wrappedValue: ViewModel(subjects: subjects))
    }

    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(alignment: .leading, spacing: 24) {
                ForEach(viewModel.subjects, id: \.id) { subject in
                    VStack(alignment: .leading, spacing: 4) {
                        Text(subject.title.capitalized)
                            .font(.custom("SFProRounded-Medium", size: 28, relativeTo: .title2))
                            .padding(.horizontal)
                            .foregroundColor(Color("TextPrimary"))

                        ScrollView(.horizontal, showsIndicators: false) {
                            LazyHStack(spacing: 16) {
                                ForEach(subject.collections) { collection in
                                    NavigationLink(destination: WordGameView(collectionGroupTitle: subject.title, collection: collection)) {
                                        WordCollectionChipView(collection: collection)
                                    }
                                }
                            }
                            .padding(.vertical, 4)
                            .padding(.horizontal)
                        }
                    }
                }
                Spacer()
            }
        }
    }
}

struct WordCollectionsView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            WordCollectionsView(subjects: [])
        }
    }
}

//
//  WordCollectionsView+ViewModel.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 04.09.2023.
//

import SwiftUI

extension WordCollectionsView {
    @MainActor final class ViewModel: ObservableObject {
        @Published var subjects: [WordCollectionSubject]

        init(subjects: [WordCollectionSubject]) {
            self.subjects = subjects
        }
    }
}

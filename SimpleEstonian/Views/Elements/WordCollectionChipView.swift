//
//  CollectionView.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 30.03.2023.
//

import SwiftUI

struct WordCollectionChipView: View {
    @StateObject private var viewModel: ViewModel

    @MainActor
    init(collection: WordCollection) {
        _viewModel = StateObject(wrappedValue: ViewModel(collection: collection))
    }

    var body: some View {
        HStack(spacing: 8) {
            Text(viewModel.collection.preview)
                .font(.body)
                .opacity(0.8)
            Text(viewModel.collection.title.capitalized)
                .font(.custom("SFProRounded-Medium", size: 17, relativeTo: .headline))
                .foregroundColor(Color("TextPrimary"))
        }
        .padding(.horizontal, 12)
        .padding(.vertical, 8)
        .cornerRadius(20)
        .overlay(
            RoundedRectangle(cornerRadius: 20)
                .stroke(Color(red: 150/255, green: 150/255, blue: 150/255), lineWidth: 1)
        )
    }
}

struct WordCollectionChipView_Previews: PreviewProvider {
    static var collection: WordCollection = {
        let c = WordCollection(id: 10, preview: "🇪🇪", title: "Flags", words: [])
        return c
    }()

    static var previews: some View {
        WordCollectionChipView(collection: collection)
    }
}

//
//  SettingsView.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 10.06.2023.
//

import SwiftUI

struct SettingsView: View {
    @Environment(\.simpleStorage) var simpleStorage: any ISimpleStorage
    @Environment(\.wordService) var wordService: any IWordStore
    @Environment(\.userSettings) var userSettings: UserSettings
    @EnvironmentObject var colorSchemeManager: ColorSchemeManager

    @StateObject var viewModel = ViewModel()

    var body: some View {
        Form {
            Section("General") {
                Link("Change your language", destination: URL(string: UIApplication.openSettingsURLString)!)
            }

            Section("Theme") {
                Toggle("Dark mode", isOn: $viewModel.isDarkModeEnabled)
                    .disabled($viewModel.useSystemSettings.wrappedValue)
                Toggle("Device settings", isOn: $viewModel.useSystemSettings)
            }
        }
        .navigationTitle("Settings")
        .onAppear {
            viewModel.setup(wordService: wordService, userSettings: userSettings, colorSchemeManager: colorSchemeManager)
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    @State static var selectedLanguage = Language(a2: "ru", name: "Russia")

    static var previews: some View {
        SettingsView()
            .environmentObject(ColorSchemeManager())
    }
}

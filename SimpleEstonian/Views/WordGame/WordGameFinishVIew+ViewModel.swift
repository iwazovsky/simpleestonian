//
//  WordGameFinishVIew+ViewModel.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 04.09.2023.
//

import SwiftUI

extension WordGameFinishView {
    @MainActor final class ViewModel: ObservableObject {
        var repeatTap: ()->Void
        var dismissTap: ()->Void

        init(repeatTap: @escaping ()->Void, dismissTap: @escaping ()->Void) {
            self.repeatTap = repeatTap
            self.dismissTap = dismissTap
        }
    }
}

//
//  WordGameFinishView.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 06.06.2023.
//

import SwiftUI

struct WordGameFinishView: View {

    @ObservedObject private var viewModel: ViewModel

    init(repeatTap: @escaping ()->Void, dismissTap: @escaping ()->Void) {
        viewModel = ViewModel(repeatTap: repeatTap, dismissTap: dismissTap)
    }

    var body: some View {
        VStack(spacing: 40) {
            Text("Great!")
                .font(.custom("SFProRounded-Bold", size: 34))

            Text("👍")
                .font(.system(size: 80))

            HStack(spacing: 24) {
                Button(action: viewModel.repeatTap, label: {
                    HStack {
                        Image(systemName: "repeat")
                            .font(.custom("SFProRounded-Bold", size: 16))
                            .foregroundColor(Color("TextPrimary"))
                        Text("Repeat")
                            .font(.custom("SFProRounded-Bold", size: 16))
                            .foregroundColor(Color("TextPrimary"))
                    }
                    .padding(.all, 12)
                    .overlay(
                        RoundedRectangle(cornerRadius: 28)
                            .stroke(Color("TextPrimary"), lineWidth: 1)
                    )
                })
                
                Button(action: viewModel.dismissTap, label: {
                    HStack {
                        Image(systemName: "arrow.backward")
                            .font(.custom("SFProRounded-Bold", size: 16))
                            .foregroundColor(Color("TextPrimary"))
                        Text("Go back")
                            .font(.custom("SFProRounded-Bold", size: 16))
                            .foregroundColor(Color("TextPrimary"))
                    }
                    .padding(.all, 12)
                    .overlay(
                        RoundedRectangle(cornerRadius: 28)
                            .stroke(Color("TextPrimary"), lineWidth: 1)
                    )
                })
            }
        }
    }
}

struct WordGameFinishView_Previews: PreviewProvider {
    static var previews: some View {
        WordGameFinishView(repeatTap: {}, dismissTap: {})
    }
}

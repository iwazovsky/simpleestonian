//
//  CollectionView.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 31.03.2023.
//

import SwiftUI

struct WordGameView: View {
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.simpleStorage) var simpleStorage
    @ObservedObject private var viewModel: ViewModel

    @MainActor
    init(collectionGroupTitle: String, collection: WordCollection) {
        viewModel = ViewModel(collectionGroupTitle: collectionGroupTitle, collection: collection)
    }

    var body: some View {
        GeometryReader { geometry in
            VStack(alignment: .center, spacing: 0) {
                WordGameControlView(collectionGroupTitle: viewModel.collectionGroupTitle, collection: viewModel.collection)
                    .frame(maxHeight: geometry.size.height/6)

                Spacer()

                ZStack {
                    Color("Purple")

                    if viewModel.isGameFinished {
                        WordGameFinishView(repeatTap: {
                            viewModel.restart()
                        }, dismissTap: {
                            presentationMode.wrappedValue.dismiss()
                        })
                    } else {
                        WordView(word: viewModel.word, showTranslation: viewModel.showTranslation) {
                            viewModel.next()
                        }
                    }

                    HStack {
                        VStack {
                            Spacer()

                            if !viewModel.isGameFinished {
                                Button(action: {
                                    withAnimation {
                                        viewModel.previous()
                                    }
                                }, label: {
                                    Circle()
                                        .frame(width: 48, height: 48)
                                        .overlay {
                                            Image(systemName: "arrow.left")
                                                .font(.system(size: 16))
                                                .foregroundColor(Color("TextPrimary"))
                                        }
                                        .foregroundColor(.none)
                                        .opacity(viewModel.wordIndex <= 0 ? 1 : 1)
                                })
                                .disabled(viewModel.wordIndex <= 0)
                            }
                        }
                        Spacer()
                    }
                    .padding(30)

                    HStack {
                        Spacer()

                        if !viewModel.isGameFinished {
                            VStack {
                                Spacer()
                                Text("\(viewModel.wordIndex+1)/\(viewModel.words.count)")
                                    .font(.custom("SFProRounded-Medium", size: 16, relativeTo: .headline))
                            }
                        }
                    }
                    .padding(44)
                }
                .cornerRadius(32)

            }
        }
        .edgesIgnoringSafeArea(.bottom)
        .navigationBarBackButtonHidden(true)
        .onAppear {
            viewModel.setup(simpleStorage: simpleStorage)
            viewModel.shuffle()
        }
    }
}

struct WordGameView_Previews: PreviewProvider {
    static var collection: WordCollection = {
        let words = [
            Word(
                id: .dumbRandom(),
                foreignWordTranslation: WordTranslation(id: .dumbRandom(), lang: "et", translation: "Some"),
                nativeWordTranslation: WordTranslation(id: .dumbRandom(), lang: "en", translation: "Some")
            ),
        ]

        let subject = WordCollectionSubject(id: .dumbRandom(), title: "Colors", collections: [])
        let collection = WordCollection(id: .dumbRandom(), preview: "Some", title: "Neutral", words: words)

        return collection
    }()

    static var previews: some View {
        NavigationView {
            WordGameView(collectionGroupTitle: "Colors", collection: collection)
        }
    }
}

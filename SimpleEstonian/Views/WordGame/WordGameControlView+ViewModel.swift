//
//  WordGameControlView+ViewModel.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 04.09.2023.
//

import SwiftUI

extension WordGameControlView {
    @MainActor final class ViewModel: ObservableObject {
        @Published var isSoundOn: Bool = false

        private var simpleStorage: (any ISimpleStorage)?

        var collectionGroupTitle: String
        var collection: WordCollection

        init(collectionGroupTitle: String, collection: WordCollection) {
            self.collectionGroupTitle = collectionGroupTitle
            self.collection = collection
        }

        func setup(simpleStorage: any ISimpleStorage) {
            self.simpleStorage = simpleStorage
            isSoundOn = simpleStorage.get("isSoundOn") ?? true
        }

        func toggleSound() {
            isSoundOn.toggle()
            simpleStorage?.set(isSoundOn, forKey: "isSoundOn")
        }
    }
}

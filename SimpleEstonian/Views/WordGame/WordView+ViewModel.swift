//
//  WordView+ViewModel.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 04.09.2023.
//

import SwiftUI

extension WordView {
    @MainActor final class ViewModel: ObservableObject {
        @Published var word: Word?
        @Published var showTranslation: Bool
        @Published var animating: Bool = false

        var animationDuration = 0.3
        var next: ()->Void

        init(word: Word?, showTranslation: Bool, next: @escaping ()->Void) {
            self.word = word
            self.showTranslation = showTranslation
            self.next = next
        }


        func tap() {
            if animating {
                return
            }

            withAnimation(.linear(duration: self.animationDuration)) {
                self.animating = true
                showTranslation.toggle()
                if !showTranslation {
                    next()
                }
                let style = showTranslation ? UIImpactFeedbackGenerator.FeedbackStyle.light : UIImpactFeedbackGenerator.FeedbackStyle.heavy
                let impact = UIImpactFeedbackGenerator(style: style)
                impact.impactOccurred()
            }

            DispatchQueue.main.asyncAfter(deadline: .now() + self.animationDuration) {
                self.animating = false
            }
        }
    }
}

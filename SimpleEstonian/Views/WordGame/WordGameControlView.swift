//
//  WordGameHeaderView.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 16.04.2023.
//

import SwiftUI

struct WordGameControlView: View {
    @StateObject var viewModel: ViewModel

    @Environment(\.presentationMode) var presentationMode
    @Environment(\.simpleStorage) var simpleStorage

    @MainActor
    init(collectionGroupTitle: String, collection: WordCollection) {
        _viewModel = StateObject(wrappedValue: ViewModel(collectionGroupTitle: collectionGroupTitle, collection: collection))
    }

    var body: some View {
        ZStack {
            HStack {
                VStack {
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Circle()
                            .strokeBorder(Color("TextPrimary"), lineWidth: 1.5)
                            .frame(width: 36, height: 36)
                            .foregroundColor(Color("TextPrimary"))
                            .overlay {
                                Image(systemName: "multiply")
                                    .foregroundColor(Color("TextPrimary"))

                            }

                    })
                    Spacer()
                }

                Spacer()
            }
            .padding(.horizontal, 36)
            .padding(.vertical, 8)

            VStack {
                VStack {
                    Text("\(viewModel.collectionGroupTitle.capitalized)")
                        .font(.custom("SFProRounded-Medium", size: 20, relativeTo: .headline))
                        .opacity(0.7)
                    Text("\(viewModel.collection.title.capitalized)")
                        .font(.custom("SFProRounded-Bold", size: 28, relativeTo: .largeTitle))
                        .foregroundColor(Color("TextPrimary"))
                }
            }
            .padding()

            HStack {
                Spacer()

                VStack {
                    Button(action: {
                        viewModel.toggleSound()
                    }, label: {
                        if viewModel.isSoundOn {
                            Image(systemName: "speaker.wave.3")
                                .frame(width: 36, height: 36)
                                .foregroundColor(Color("TextPrimary"))

                        } else {
                            Image(systemName: "speaker.slash")
                                .frame(width: 36, height: 36)
                                .foregroundColor(Color("TextPrimary"))
                        }
                    })

                    Spacer()
                }
                
            }
            .padding(.horizontal, 36)
            .padding(.vertical, 8)

        }
        .onAppear {
            viewModel.setup(simpleStorage: simpleStorage)
        }
    }
}

struct WordGameControlView_Previews: PreviewProvider {
    static var collection: WordCollection = {
        let words = [
            Word(
                id: .dumbRandom(),
                foreignWordTranslation: WordTranslation(id: .dumbRandom(), lang: "et", translation: "Some"),
                nativeWordTranslation: WordTranslation(id: .dumbRandom(), lang: "en", translation: "Some")
            ),
        ]

        let collection = WordCollection(id: .dumbRandom(), preview: "Some", title: "Neutral", words: words)

        return collection
    }()

    static var previews: some View {
        WordGameControlView(collectionGroupTitle: "Colors", collection: collection)

    }
}

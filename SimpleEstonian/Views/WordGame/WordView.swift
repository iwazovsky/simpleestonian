//
//  WordView.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 14.04.2023.
//

import SwiftUI

struct WordView: View {
    @Environment(\.simpleStorage) var simpleStorage

    @ObservedObject private var viewModel: ViewModel

    @MainActor
    init(word: Word?, showTranslation: Bool, next: @escaping ()->Void) {
        viewModel = ViewModel(word: word, showTranslation: showTranslation, next: next)
    }

    var body: some View {
        VStack {
            Spacer()
            Text(viewModel.word?.nativeWordTranslation.translation ?? "")
                .font(.custom("SFProRounded-Bold", size: 36, relativeTo: .largeTitle))
                .foregroundColor(Color("TextPrimary"))

            if viewModel.showTranslation {
                Spacer()
                Image(systemName: "arrow.down")
                    .foregroundColor(Color("TextPrimary"))

                Spacer()
                Text(viewModel.word?.foreignWordTranslation.translation ?? "")
                    .font(.custom("SFProRounded-Bold", size: 36, relativeTo: .largeTitle))
                    .foregroundColor(Color("TextPrimary"))
            }

            Spacer()
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .contentShape(Rectangle())
        .font(.title)
        .foregroundColor(.primary)
        .onTapGesture {
            viewModel.tap()
        }
    }
}

struct WordView_Previews: PreviewProvider {
    static var word: Word = {
        let foreignWordTranslation = WordTranslation(id: Int.random(in: 1...Int.max), lang: "en", translation: "Some")
        let nativeWordTranslation = WordTranslation(id: Int.random(in: 1...Int.max), lang: "et", translation: "Some")
        let word = Word(id: Int.random(in: 1...Int.max), foreignWordTranslation: foreignWordTranslation, nativeWordTranslation: nativeWordTranslation)
        return word
    }()

    static var previews: some View {
        WordView(word: word, showTranslation: false, next: {})
    }
}

//
//  WordGameView+ViewModel.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 04.09.2023.
//

import SwiftUI
import AVFoundation

extension WordGameView {
    @MainActor final class ViewModel: ObservableObject {
        @Published var words: [Word] = []
        @Published var word: Word? = nil
        @Published var showTranslation = false
        @Published var wordIndex = 0
        @Published var isGameFinished = false
        @Published var isLongPressing = false

        private var simpleStorage: (any ISimpleStorage)?

        var audioPlayer: AVAudioPlayer?

        var collectionGroupTitle: String
        var collection: WordCollection

        init(collectionGroupTitle: String, collection: WordCollection) {
            self.collectionGroupTitle = collectionGroupTitle
            self.collection = collection

            words = collection.words
            wordIndex = 0

            if let word = self.words.first {
                self.word = word
            }
        }

        func setup(simpleStorage: (any ISimpleStorage)?) {
            self.simpleStorage = simpleStorage
        }

        func restart() {
            shuffle()
            isGameFinished = false
            showTranslation = false
        }

        func next() {
            if wordIndex+1 >= words.count {
                gameIsFinished()
            } else {
                playNextWordSound()
                wordIndex += 1
                word = words[wordIndex]
            }
        }

        func gameIsFinished() {
            playGameFinishSound()
            isGameFinished = true
        }

        func previous() {
            if wordIndex > 0 {
                wordIndex -= 1
                word = words[wordIndex]
                showTranslation = false
            }
        }

        func shuffle() {
            words = collection.words.shuffled()
            if let word = words.first {
                self.word = word
            }
            wordIndex = 0
        }

        func playNextWordSound() {
            guard let isSoundOn: Bool = simpleStorage?.get("isSoundOn"), isSoundOn else { return }

            if let path = Bundle.main.path(forResource: "wordFinishSound", ofType: "mp3") {
                do {
                    audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
                    audioPlayer?.play()
                } catch {
                    print("Error \(error.localizedDescription)")
                }
            }
        }

        func playGameFinishSound() {
            guard let isSoundOn: Bool = simpleStorage?.get("isSoundOn"), isSoundOn else { return }

            if let path = Bundle.main.path(forResource: "gameFinishSound", ofType: "mp3") {
                do {
                    audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
                    audioPlayer?.play()
                } catch {
                    print("Error \(error.localizedDescription)")
                }
            }
        }
    }
}

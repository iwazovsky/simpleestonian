//
//  ContentView+ViewModel.swift
//  SimpleEstonian
//
//  Created by Konstantin Tukmakov on 04.09.2023.
//

import SwiftUI

extension ContentView {
    @MainActor final class ViewModel: ObservableObject {
        private var wordService: (any IWordStore)?
        @Published var subjects: [WordCollectionSubject] = []
        
        func setup(wordService: any IWordStore) {
            self.wordService = wordService
            fetchSubjects()
        }

        func fetchSubjects() {
            subjects = wordService?.getSubjects() ?? []
        }

    }
}

